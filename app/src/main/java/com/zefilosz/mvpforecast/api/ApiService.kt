package com.zefilosz.mvpforecast.api

import com.zefilosz.mvpforecast.model.Repository
import com.zefilosz.mvpforecast.model.Weather
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("users/{username}/repos")
    fun getRepositories(@Path("username") username: String): Observable<List<Repository>>

    @GET ("weather")
    fun getWeather(@Query("q") q: String,
                   @Query("appid") appid: String): Observable<Weather>
}