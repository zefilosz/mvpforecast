package com.zefilosz.mvpforecast.viewmodel

interface BaseViewModel {
    fun destroy()
}