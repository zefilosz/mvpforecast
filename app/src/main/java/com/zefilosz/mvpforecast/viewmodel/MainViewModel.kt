package com.zefilosz.mvpforecast.viewmodel

import android.content.Context
import android.databinding.ObservableBoolean
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.zefilosz.mvpforecast.binding.TextWatcherAdapter
import com.zefilosz.mvpforecast.model.Repository
import com.zefilosz.mvpforecast.model.Weather
import com.zefilosz.mvpforecast.repository.MainRepository
import com.zefilosz.mvpforecast.repository.WeatherRepository
import io.reactivex.observers.DisposableObserver

class MainViewModel (var context: Context?, var repositoryListener: RepositoryListener): BaseViewModel, WeatherRepository.WeatherDataListener {
    override fun onSuccess(value: Weather?) {
        this.repositories2 = value
    }

    var isSearch: ObservableBoolean = ObservableBoolean(false)
    //private var mainRespository: MainRepository = MainRepository(context!!, this)
    private var mainRespository: WeatherRepository = WeatherRepository(context!!, this)
    lateinit var editTextCityNameValue: String
    private var disposable: DisposableObserver<Weather>? = null
    private var repositories: List<Repository>? = null
    private var repositories2: Weather? = null

    interface RepositoryListener {
        fun  onRepositoriesChanged(repositories: List<Repository>)
    }

    fun onClickSearch(view: View) {
        //loadRepositories(editTextCityNameValue)
        loadWeatherRepositories(editTextCityNameValue)
        //Toast.makeText(context, editTextCityNameValue, Toast.LENGTH_SHORT).show()
    }

    fun getCityNameEditTextWatcher(): TextWatcher {
        return object : TextWatcherAdapter() {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                super.onTextChanged(s, start, before, count)
                editTextCityNameValue = s.toString()
                isSearch.set(s?.length!! > 0)

                Log.d("test", s.toString())
            }
        }
    }

//    fun loadRepositories(username : String) {
//        disposable  = mainRespository.loadRepositories(username)
//    }

    fun loadWeatherRepositories(username : String) {
        disposable  = mainRespository.loadRepositories(username)
    }

    override fun onError(message: String) {
    }

    override fun onComplete() {
//        repositoryListener?.onRepositoriesChanged(repositories!!)
//
//        if (!repositories?.isEmpty()!!){
//
//        } else {
//
//        }
    }

//    override fun onSuccess(value: List<Repository>?) {
//        //this.repositories = value
//
//    }

    override fun destroy() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}