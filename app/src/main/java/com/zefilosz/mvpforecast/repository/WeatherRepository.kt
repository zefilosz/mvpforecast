package com.zefilosz.mvpforecast.repository

import android.content.Context
import com.zefilosz.mvpforecast.App
import com.zefilosz.mvpforecast.api.ApiService
import com.zefilosz.mvpforecast.api.ServiceFactory
import com.zefilosz.mvpforecast.model.Repository
import com.zefilosz.mvpforecast.model.Weather
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver

class WeatherRepository (var context: Context, var dataListener: WeatherDataListener) {

    interface WeatherDataListener {
        fun onError(message: String)
        fun onComplete()
        fun onSuccess(value: Weather?)
    }

    private var factory: ServiceFactory = ServiceFactory()
    private var api: ApiService = factory.provideApi()

    /**
     * load repositories
     */
    fun loadRepositories(username : String): DisposableObserver<Weather>? {
        return api.getWeather("bangkok", "b24247acf890484c044f951cde75c7bf")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(App.instance?.defaultSubscribeScheduler())
                .subscribeWith(WeatherObserver())
    }

    private inner class WeatherObserver: DisposableObserver<Weather>() {

        override fun onComplete() {
            dataListener.onComplete()
        }

        override fun onError(e: Throwable?) {
            //dataListener.onError(context?.getString(R.string.error_username_not_found))
            var x = e.toString()
        }

        override fun onNext(value: Weather?) {
            dataListener.onSuccess(value)
        }

    }
}