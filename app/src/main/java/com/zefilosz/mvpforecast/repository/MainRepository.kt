package com.zefilosz.mvpforecast.repository

import android.content.Context
import com.zefilosz.mvpforecast.App
import com.zefilosz.mvpforecast.api.ApiService
import com.zefilosz.mvpforecast.api.ServiceFactory
import com.zefilosz.mvpforecast.model.Repository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver

class MainRepository (var context: Context, var dataListener: DataListener) {

    interface DataListener {
        fun onError(message: String)
        fun onComplete()
        fun onSuccess(value: List<Repository>?)
    }

    private var factory: ServiceFactory = ServiceFactory()
    private var api: ApiService = factory.provideApi()

    /**
     * load repositories
     */
    fun loadRepositories(username : String): DisposableObserver<List<Repository>>? {
        return api.getRepositories(username)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(App.instance?.defaultSubscribeScheduler())
                .subscribeWith(LongOperationObserver())
    }

    private inner class LongOperationObserver: DisposableObserver<List<Repository>>() {

        override fun onComplete() {
            dataListener.onComplete()
        }

        override fun onError(e: Throwable?) {
            //dataListener.onError(context?.getString(R.string.error_username_not_found))
        }

        override fun onNext(value: List<Repository>?) {
            dataListener.onSuccess(value)
        }

    }
}