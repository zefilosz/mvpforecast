package com.zefilosz.mvpforecast.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.zefilosz.mvpforecast.R
import com.zefilosz.mvpforecast.viewmodel.MainViewModel
import com.zefilosz.mvpforecast.databinding.ActivityMainBinding
import com.zefilosz.mvpforecast.model.Repository
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainViewModel.RepositoryListener {
    override fun onRepositoriesChanged(repositories: List<Repository>) {
        //
    }

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    private fun activityView() {
        binding.editTextUsername?.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val username = binding.editTextUsername.text
                    if (username.isNotEmpty())
                        viewModel.loadWeatherRepositories(username.toString())
                    return true
                }
                return false
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupBinding()
        setSupportActionBar(toolbar)
        activityView()
    }

    private fun setupBinding() {
        viewModel   = MainViewModel(this, this)
        binding.vm  = viewModel
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
