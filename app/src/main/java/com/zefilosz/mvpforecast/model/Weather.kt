package com.zefilosz.mvpforecast.model

import com.google.gson.annotations.SerializedName

data class Weather (
        val id: String,
        val name: String
)