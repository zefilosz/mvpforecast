package com.zefilosz.mvpforecast

import io.restassured.RestAssured.given
import io.restassured.builder.RequestSpecBuilder
import io.restassured.filter.log.RequestLoggingFilter
import io.restassured.filter.log.ResponseLoggingFilter
import io.restassured.http.ContentType
import io.restassured.specification.RequestSpecification
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Test

class ServiceWeatherTest {

    private var spec: RequestSpecification = RequestSpecBuilder().
            setContentType(ContentType.JSON).
            setBaseUri("http://api.openweathermap.org/data/2.5/").
            addFilter(ResponseLoggingFilter()).
            addFilter(RequestLoggingFilter()).build()

    @Test
    fun provideCurrentWeatherApi(){
        val mapKey = mutableMapOf("q" to "bangkok", "appid" to "b24247acf890484c044f951cde75c7bf")

        given().spec(spec)
            .params(mapKey)
            .get("weather")
            .then()
            .statusCode(200)
            .assertThat()
            .body("name", equalTo("Bangkok"))
    }

    private var spec2: RequestSpecification = RequestSpecBuilder().
            setContentType(ContentType.JSON).
            setBaseUri("http://api.openweathermap.org/data/2.5/").
            addFilter(ResponseLoggingFilter()).
            addFilter(RequestLoggingFilter()).build()

    @Test
    fun provideWholeWeatherApi(){
        val mapKey = mutableMapOf("id" to "1609350", "appid" to "b24247acf890484c044f951cde75c7bf")

        given().spec(spec2)
                .params(mapKey)
                .get("forecast")
                .then()
                .statusCode(200)
                .assertThat()
                .body("city.name", equalTo("Bangkok"))
    }

    @After
    fun tearDown() {
    }
}